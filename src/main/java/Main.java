import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Main {
    public static void main(String[] args) {

        printBonusDatesBetween(2010, 2015);

    }

    static void printBonusDatesBetween(int fromYear, int toYear) {

        LocalDate startDate = LocalDate.of(fromYear, 1, 1);
        LocalDate endDate = LocalDate.of(toYear, 1, 1);

        while (startDate.isBefore(endDate)) {
            String formattedDate = startDate.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
            StringBuilder stringBuilder = new StringBuilder(formattedDate);
            String reversedDate = stringBuilder.reverse().toString();
            if (reversedDate.equals(formattedDate)) {
                System.out.println(startDate);
            }
            startDate = startDate.plusDays(1L);
        }
    }
}




